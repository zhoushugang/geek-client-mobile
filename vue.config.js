module.exports = {
  css: {
    loaderOptions: {
      less: {
        globalVars: {
          'geek-color': '#FC6627',
          'geek-gray-color': '#F7F8FA'
        }
      }
    }
  }
}

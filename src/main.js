import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Vant from 'vant'
import 'vant/lib/index.css'
// 全局样式
import '@/assets/styles/common.less'
// 项目插件
import Geek from '@/components'
// 使用插件
Vue.use(Geek)
Vue.use(Vant)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
